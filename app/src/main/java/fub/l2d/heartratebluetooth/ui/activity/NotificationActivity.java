package fub.l2d.heartratebluetooth.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class NotificationActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent reorderIntent = new Intent(this, RecordActivity.class);
        reorderIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(reorderIntent);
        finish();
    }
}
