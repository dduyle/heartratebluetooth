package fub.l2d.heartratebluetooth.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import fub.l2d.heartratebluetooth.controller.CSVController;
import fub.l2d.heartratebluetooth.controller.RealmController;
import fub.l2d.heartratebluetooth.model.RR;
import io.realm.Realm;
import io.realm.RealmResults;

public class AlarmReceiver extends BroadcastReceiver {
    public AlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                CSVController csvController = new CSVController();
                csvController.writeToFile(false, null);
            }
        }).start();
    }

}
