package fub.l2d.heartratebluetooth.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.ArraySet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by duc-d on 2/13/2017.
 */

public class PreferenceUtils {
    public static final String FAVORITE = "l2d.rrlogger.favorite";

    public static final void addFav(Context context, List<String> favs) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putStringSet(FAVORITE, new HashSet<String>(favs));
        editor.commit();
    }


    public static final List<String> getFavs(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> addrs = sharedPreferences.getStringSet(FAVORITE, new HashSet<String>());
        List<String> result = new ArrayList<>(addrs);
        return result;
    }


    private static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        return editor;
    }
}
