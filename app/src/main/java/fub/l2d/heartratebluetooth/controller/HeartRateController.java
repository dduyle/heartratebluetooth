package fub.l2d.heartratebluetooth.controller;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import fub.l2d.heartratebluetooth.model.RR;
import fub.l2d.heartratebluetooth.service.BluetoothLeService;
import fub.l2d.heartratebluetooth.util.SampleGattAttributes;

/**
 * Created by duc-d on 2/9/2017.
 */

public class HeartRateController {
    public BluetoothGattService heartRateService;
    public BluetoothGattCharacteristic heartRateCharacteristic;

    public BluetoothGattService batteryService;
    public BluetoothGattCharacteristic batteryCharacteristic;

    public BluetoothLeService bleService;
    public String patientName;
    public Date patientDate;
    public long duration;

    public String filePath;

    public List<RR> rrs;

    public HeartRateController() {
        if (rrs == null) {
            rrs = new ArrayList<>();
        }
    }

    public void initializeBluetoothGattCharacteristic(List<BluetoothGattService> gattServices) {
        for (BluetoothGattService gattService : gattServices) {
            final List<BluetoothGattCharacteristic> gattCharacteristics = gattService
                    .getCharacteristics();

            Log.e("uuid", gattService.getUuid().toString());
            if (gattService.getUuid().equals(
                    UUID.fromString(SampleGattAttributes.HEART_RATE_SERVICE))) {
                heartRateService = gattService;
                heartRateCharacteristic = gattCharacteristics.get(0);
            }

            if (gattService.getUuid().equals(UUID.fromString(SampleGattAttributes.BATTERY_SERVICE))) {
                batteryService = gattService;
                batteryCharacteristic = gattCharacteristics.get(0);
            }
        }
    }

    public void enableHeartRateSensor() {
        if (heartRateCharacteristic != null) {
            bleService.setCharacteristicNotification(heartRateCharacteristic, true);
            bleService.readCharacteristic(heartRateCharacteristic);
        }
    }

    public void readBattery() {
        if (batteryCharacteristic != null) {
            Log.e("Read Battery", "initialized reader");
            bleService.readCharacteristic(batteryCharacteristic);
        }
    }


}
