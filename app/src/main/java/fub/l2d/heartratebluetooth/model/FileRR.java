package fub.l2d.heartratebluetooth.model;

import java.io.Serializable;

/**
 * Created by duc-d on 2/13/2017.
 */

public class FileRR implements Serializable {
    public String fullPath;
    public String filename;
    public boolean checked;
    public long modified;

    public FileRR(String filename) {
        this.filename = filename;
    }
}
