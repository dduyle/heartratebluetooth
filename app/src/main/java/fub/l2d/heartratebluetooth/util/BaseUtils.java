package fub.l2d.heartratebluetooth.util;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import fub.l2d.heartratebluetooth.model.RR;

/**
 * Created by duc-d on 2/10/2017.
 */

public class BaseUtils {
    private static final String TAG = BaseUtils.class.getSimpleName();

    public static final String APP_FOLDER = "RR LOGGER";
    public static final String FILES = APP_FOLDER + File.separator + "files/";
    public static final String TEMP = APP_FOLDER + File.separator + "temp/";

    public static final String HOUR_FORMAT = "HH:mm:ss";
    public static final String TODAY_DATE_FORMAT = "dd MMMM yyyy";
    public static final String FULL_DAY = "dd MMMM yyyy à HH:mm:ss";
    public static final String DAY_FILE_NAME = "dd_MM_yyyy";

    public static final String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(HOUR_FORMAT);
        return sdf.format(date);
    }

    public static final String fullDay(Date date) {
        if (date == null) {
            return "";
        }

        SimpleDateFormat sdf = new SimpleDateFormat(FULL_DAY);
        return sdf.format(date);
    }

    public static final String formatToday(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(TODAY_DATE_FORMAT);
        return sdf.format(date);
    }

    public static final Date fromLong(long timestamp) {
        return new Date(timestamp);
    }

    public static boolean isHeartRateService(BluetoothGattCharacteristic characteristic) {
        return SampleGattAttributes.HEART_RATE_SERVICE.equals(characteristic.getService().getUuid().toString());
    }

    public static boolean isBatteryService(BluetoothGattCharacteristic characteristic) {
        return SampleGattAttributes.BATTERY_SERVICE.equals(characteristic.getService().getUuid().toString());
    }

    public static String dateToSave(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(DAY_FILE_NAME);
        return sdf.format(date);
    }

    /**
     * https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.characteristic.heart_rate_measurement.xml&u=org.bluetooth.characteristic.heart_rate_measurement.xml
     * the first bit is Heart rate value
     *
     * @param characteristic
     * @return
     */
    public static RR[] extractBeatToBeatInterval(
            BluetoothGattCharacteristic characteristic) {

        int flag = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        int format = -1;
        int energy = -1;
        int offset = 1; // This depends on hear rate value format and if there is energy data
        int rr_count = 0;

        if ((flag & 0x01) != 0) { // UINT16
            offset = 3;
        } else { // UINT8
            offset = 2;
        }

        if ((flag & 0x08) != 0) { //
            offset += 2;
        }

        if ((flag & 0x16) != 0) {
            // RR stuff.
            rr_count = ((characteristic.getValue()).length - offset) / 2;
            if (rr_count > 0) {
                RR[] mRr_values = new RR[rr_count];
                for (int i = 0; i < rr_count; i++) {
                    int rr = characteristic.getIntValue(
                            BluetoothGattCharacteristic.FORMAT_UINT16, offset);
                    offset += 2;
                    RR rrObject = new RR(rr, System.currentTimeMillis());
                    mRr_values[i] = rrObject;
//                    Log.e(TAG, "rr value " + rr);
                }
                return mRr_values;
            }
        }
        return null;
    }

    public static int extractBatteryLevel(BluetoothGattCharacteristic characteristic) {
        return characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
    }
}
