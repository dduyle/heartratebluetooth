package fub.l2d.heartratebluetooth.ui.activity;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import fub.l2d.heartratebluetooth.R;
import fub.l2d.heartratebluetooth.adapter.RRAdapter;
import fub.l2d.heartratebluetooth.controller.CSVController;
import fub.l2d.heartratebluetooth.controller.HeartRateController;
import fub.l2d.heartratebluetooth.controller.RealmController;
import fub.l2d.heartratebluetooth.model.Battery;
import fub.l2d.heartratebluetooth.model.Device;
import fub.l2d.heartratebluetooth.model.RR;
import fub.l2d.heartratebluetooth.service.AlarmReceiver;
import fub.l2d.heartratebluetooth.service.BluetoothLeService;
import fub.l2d.heartratebluetooth.util.BaseUtils;
import fub.l2d.heartratebluetooth.util.PreferenceUtils;
import io.fabric.sdk.android.Fabric;

public class RecordActivity extends AppCompatActivity {

    public static final int NOTIF_ID = 10000;
    public static final int REQUEST_CODE = 12343;

    private final static String TAG = RecordActivity.class.getSimpleName();
    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    public static final String EXTRAS_DEVICE_NAME = "l2d.hrble.device.patientName";
    public static final String EXTRAS_DEVICE_ADDRESS = "l2d.hrble.device.addr";

    private BluetoothLeService bluetoothLeService;
    private Device device;
    private boolean connected;
    private boolean connectCalled;
    private HeartRateController heartRateController;

    private RecyclerView rrsView;
    private RRAdapter adapter;

    private Button btnStop;
    private boolean isRecording;

    private TextView patientNameView;
    private TextView patientDateView;
    private TextView durationView;

    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;

    private static final int INTERVAL = 10 * 1000;

    private long rrsCount;
    private ImageView openFolderView;
    private ImageView favView;
    private Handler customHandler = new Handler();


    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            updateDuration();
            customHandler.postDelayed(this, 1000);
        }
    };

    // Code to manage Service lifecycle.
    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            bluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!bluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                connected = true;
                snackLong(getString(R.string.connected));
                heartRateController.bleService = bluetoothLeService;
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                connected = false;
                snackLong(getString(R.string.deconnected));
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                heartRateController.initializeBluetoothGattCharacteristic(bluetoothLeService.getSupportedGattServices());
                heartRateController.readBattery();
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Intent intent = getIntent();
        String deviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        String deviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        device = new Device(deviceName, deviceAddress);


        TextView title = (TextView) findViewById(R.id.title);
        title.setText(Html.fromHtml(deviceName));

        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, serviceConnection, BIND_AUTO_CREATE);

        rrsView = (RecyclerView) findViewById(R.id.last_datas);
        rrsView.setLayoutManager(new LinearLayoutManager(this));

        btnStop = (Button) findViewById(R.id.btn_record);
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRecordListener();
            }
        });

        heartRateController = new HeartRateController();
        adapter = new RRAdapter(heartRateController.rrs);
        rrsView.setAdapter(adapter);

        patientNameView = (TextView) findViewById(R.id.patient_name);
        patientDateView = (TextView) findViewById(R.id.patient_date);
        durationView = (TextView) findViewById(R.id.record_duration);

        EventBus.getDefault().register(this);
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        rrsCount = 0;

        favView = (ImageView) findViewById(R.id.fav);
        openFolderView = (ImageView) findViewById(R.id.open);

        favoritesInit();

        favView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favoriteClick();
            }
        });

        openFolderView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileExplorer();
            }
        });
    }

    private void favoritesInit() {
        List<String> favs = PreferenceUtils.getFavs(getApplicationContext());
        if (favs.contains(device.addr)) {
            favView.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite));
        } else {
            favView.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_unfavo));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        unbindService(serviceConnection);
        unregisterReceiver(mGattUpdateReceiver);
        if (bluetoothLeService != null) {
            bluetoothLeService.disconnect();
            bluetoothLeService = null;
        }
        EventBus.getDefault().unregister(this);
        cancelAlarmAndWriteFile(null);
        super.onDestroy();
    }

    private void cancelAlarmAndWriteFile(final Handler handler) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                new CSVController().writeToFile(true, handler);
            }
        }).start();

        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
            alarmManager = null;
            pendingIntent = null;
        }
    }

    // Here received rrs value from bluetoothleService
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRRSEvent(RR[] rrs) {
        if (!isRecording) {
            return;
        }

        rrsCount += rrs.length;
        for (RR rr : rrs) {
            rr.fileName = heartRateController.filePath;
        }

        new RealmController().saveRRs(Arrays.asList(rrs));

        for (RR rr : heartRateController.rrs) {
            rr.isNewValue = false;
        }

        heartRateController.rrs.addAll(0, Arrays.asList(rrs));
        if (heartRateController.rrs.size() > 6) {
            while (heartRateController.rrs.size() > 6) {
                heartRateController.rrs.remove(6);
            }
        }

        adapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBattertEvent(Battery battery) {
        Log.e("Battery level", Integer.toString(battery.battery));
        final Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getString(R.string.battery_level, Integer.toString(battery.battery)) + "%", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Effacer", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();

        heartRateController.enableHeartRateSensor();
    }

    private void snackLong(String content) {
        Snackbar.make(findViewById(android.R.id.content), content, Snackbar.LENGTH_LONG).show();
    }

    private void snackShort(String content) {
        Snackbar.make(findViewById(android.R.id.content), content, Snackbar.LENGTH_SHORT).show();
    }

    /**
     * Add filter actions
     *
     * @return
     */
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    private void btnRecordListener() {

        if (TextUtils.isEmpty(heartRateController.patientName)) {
            inviteToInputUsername();
            return;
        }

        if (!isRecording) { // Begin recording
            if (!connectCalled) {
                bluetoothLeService.connect(device.addr);
            }
            btnStop.setBackground(ContextCompat.getDrawable(this, R.drawable.round_button_stop));
            btnStop.setText(getString(R.string.stop));
            isRecording = true;
        } else { // Finish recording
            // dialog confirm
            new MaterialDialog.Builder(this)
                    .title(R.string.exit_title)
                    .positiveText(R.string.yes)
                    .negativeText(R.string.no)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            finishRecording();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }

    private void finishRecording() {
        snackLong(Long.toString(rrsCount));
        btnStop.setBackground(ContextCompat.getDrawable(this, R.drawable.round_button_start));
        btnStop.setText(getString(R.string.restart));
        heartRateController.patientDate = null;
        heartRateController.patientName = "";
        isRecording = false;

        updateInterface();

        bluetoothLeService.disconnect();
        connectCalled = false;

        final MaterialDialog dialog = new MaterialDialog.Builder(this).title(R.string.writing_file_title)
                .content(R.string.writing_file_content)
                .cancelable(false)
                .progress(true, 0)
                .build();
        dialog.show();

        cancelAlarmAndWriteFile(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == CSVController.FINISH_CSV_FILE) {
                    dialog.dismiss();
                    snackLong(getString(R.string.saved));
                }
            }
        });


        customHandler.removeCallbacks(updateTimerThread);
    }

    @Override
    protected boolean onPrepareOptionsPanel(View view, Menu menu) {
        if (menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod(
                            "setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName(), "onMenuOpened...unable to set icons for overflow menu", e);
                }
            }
        }
        return super.onPrepareOptionsPanel(view, menu);
    }

    @Override
    public void onBackPressed() {
        if (!isRecording) {
            super.onBackPressed();
        } else {
//            snackLong(getString(R.string.cannot_exit));
            Intent setIntent = new Intent(Intent.ACTION_MAIN);
            setIntent.addCategory(Intent.CATEGORY_HOME);
            setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(setIntent);
            createNotification();
        }
    }

    private void createNotification() {
        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(getString(R.string.on_going))
                        .setAutoCancel(true)
                        .setOngoing(true)
                        .setContentText(getString(R.string.on_going_deivce, device.name));

        Intent intent = new Intent(this, NotificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                intent, 0);
        mBuilder.setContentIntent(pendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIF_ID, mBuilder.build());
    }

    private void favoriteClick() {
        List<String> favs = PreferenceUtils.getFavs(getApplicationContext());
        if (favs.contains(device.addr)) {
            // remove favoriteClick
            favs.remove(device.addr);
            favView.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_unfavo));
        } else {
            // add favoriteClick
            favs.add(device.addr);
            favView.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite));
        }

        PreferenceUtils.addFav(getApplicationContext(), favs);
    }

    private void inviteToInputUsername() {
        new MaterialDialog.Builder(this)
                .title(R.string.invite_username_title)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(R.string.invite_username_hint, R.string.invite_username_prefill, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        if (TextUtils.isEmpty(input)) {
                            snackShort(getString(R.string.invite_username_error));
                        } else {
                            heartRateController.rrs.clear();
                            adapter.notifyDataSetChanged();
                            heartRateController.patientName = input.toString();
                            heartRateController.patientDate = new Date();
                            updateInterface();
                            dialog.dismiss();

                            btnStop.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.round_button_stop));
                            btnStop.setText(getString(R.string.stop));

                            isRecording = true;

                            if (!connectCalled) {
                                bluetoothLeService.connect(device.addr);
                                connectCalled = true;
                            }

                            Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);

                            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                            long firstSetup = System.currentTimeMillis();
                            alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstSetup, INTERVAL, pendingIntent);

                            createFileName();

                            heartRateController.duration = 0;
                            customHandler.postDelayed(updateTimerThread, 0);
                        }
                    }
                }).show();
    }

    private void openFileExplorer() {
        Intent intent = new Intent(this, FileBrowserActivity.class);
        startActivity(intent);
    }

    private void updateInterface() {
        patientNameView.setText(heartRateController.patientName);
        patientDateView.setText(getString(R.string.begin_recording, BaseUtils.formatToday(new Date())));
    }

    private void updateDuration() {
        heartRateController.duration++;
        long seconds = heartRateController.duration;
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);

        durationView.setText(getString(R.string.duration, Long.toString(day), Long.toString(hours), Long.toString(minute), Long.toString((second))));
    }

    private void createFileName() {
        String extension = ".csv";
        String filename = heartRateController.patientName + "_" + BaseUtils.dateToSave(heartRateController.patientDate);
        String filePath = android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + BaseUtils.FILES + filename;
        File file = new File(filePath + extension);
        if (file.exists()) {
            int i = 1;
            boolean result = false;
            while (!result) {
                file = new File(filePath + "_" + i + extension);
                if (!file.exists()) {
                    result = true;
                    heartRateController.filePath = filePath + "_" + i + extension;
                }
            }
        } else {
            heartRateController.filePath = filePath + extension;
        }

        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            Fabric.getLogger().e(TAG, e.getMessage());
        }
    }
}
