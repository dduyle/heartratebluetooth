package fub.l2d.heartratebluetooth.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import fub.l2d.heartratebluetooth.R;
import fub.l2d.heartratebluetooth.adapter.FileAdapter;
import fub.l2d.heartratebluetooth.model.FileRR;
import fub.l2d.heartratebluetooth.util.BaseUtils;

public class FileBrowserActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    private RecyclerView filesView;
    private List<FileRR> files;
    private FileAdapter fileAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_browser);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        filesView = (RecyclerView) findViewById(R.id.filesview);
        filesView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        getData();
    }

    public void showPopup(View v) {

        final ViewGroup root = (ViewGroup) getWindow().getDecorView().findViewById(android.R.id.content);

        final View view = new View(getApplicationContext());
        view.setLayoutParams(new ViewGroup.LayoutParams(1, 1));
        view.setBackgroundColor(Color.TRANSPARENT);

        root.addView(view);

        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }

        view.setX(root.getWidth() - getResources().getDimensionPixelSize(R.dimen.button_small_padding));
        view.setY(result + getResources().getDimensionPixelSize(R.dimen.button_small_padding));

        PopupMenu popup = new PopupMenu(this, view);
        popup.setGravity(Gravity.RIGHT | Gravity.TOP);

        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }


    private void getData() {

        String path = Environment.getExternalStorageDirectory() + File.separator + BaseUtils.FILES;

        File directory = new File(path);
        if (directory.exists()) {
            File[] files = directory.listFiles();
            List<FileRR> filePaths = new ArrayList<>();
            if (files != null) {
                for (File file : files) {
                    FileRR fileRR = new FileRR(file.getName());
                    fileRR.modified = file.lastModified();
                    fileRR.fullPath = file.getAbsolutePath();
                    filePaths.add(fileRR);
                }
            }

            this.files = filePaths;
            if (this.files != null) {
                Collections.sort(this.files, new Comparator<FileRR>() {
                    @Override
                    public int compare(FileRR o1, FileRR o2) {
                        return o1.filename.compareTo(o2.filename);
                    }
                });
            }

            fileAdapter = new FileAdapter(this.files);
            filesView.setAdapter(fileAdapter);
        }

        if (files == null || files.size() == 0) {
            filesView.setVisibility(View.GONE);
            findViewById(R.id.empty_file).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                delete();
                return true;
            case R.id.share:
                share();
                return true;
        }
        return false;
    }

    private void share() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND_MULTIPLE);
        intent.setType("text/csv"); /* This example is sharing jpeg images. */

        ArrayList<Uri> files = new ArrayList<Uri>();

        List<String> filesToSend = new ArrayList<>();
        for (FileRR f : this.files) {
            if (f.checked) {
                filesToSend.add(f.fullPath);
            }
        }

        if (filesToSend.size() > 0) {

            for (String path : filesToSend) {
                File file = new File(path);
                Uri uri = null;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    uri = FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".provider", file);
                } else {
                    uri = Uri.fromFile(file);
                }

                files.add(uri);
            }

            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);
            startActivity(intent);
        }
    }

    private void delete() {
        final List<FileRR> dels = new ArrayList<>();
        for (FileRR f : files) {
            if (f.checked) {
                dels.add(f);
            }
        }

        if (dels.size() > 0) {
            new MaterialDialog.Builder(this)
                    .title(R.string.delete_title)
                    .positiveText(R.string.yes)
                    .negativeText(R.string.no)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            for (FileRR f : dels) {
                                File file = new File(f.fullPath);
                                if (file.exists()) {
                                    file.delete();
                                }
                            }

                            files.removeAll(dels);
                            fileAdapter.notifyDataSetChanged();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }
}
