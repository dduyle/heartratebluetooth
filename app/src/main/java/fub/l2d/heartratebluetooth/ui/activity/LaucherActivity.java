package fub.l2d.heartratebluetooth.ui.activity;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.crashlytics.android.Crashlytics;

import java.io.File;
import java.util.List;

import fub.l2d.heartratebluetooth.R;
import fub.l2d.heartratebluetooth.adapter.BLEDevicesAdapter;
import fub.l2d.heartratebluetooth.controller.CSVController;
import fub.l2d.heartratebluetooth.ui.iviews.ILaucher;
import fub.l2d.heartratebluetooth.util.BaseUtils;
import fub.l2d.heartratebluetooth.util.PreferenceUtils;
import io.fabric.sdk.android.Fabric;

public class LaucherActivity extends AppCompatActivity implements ILaucher {

    private static final String TAG = LaucherActivity.class.getSimpleName();

    private static final int REQUEST_ENABLE_BT = 100;
    private static final int REQUEST_ENABLE_LOCATION = 200;

    private static final int PERMISSION_ALL = 1;
    private static final long SCAN_PERIOD = 60000 * 5;
    private static final String[] PERMISSIONS = {Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION};

    private BLEDevicesAdapter devicesAdapter;
    private BluetoothAdapter bluetoothAdapter;
    private Handler handler;
    private ScanCallback scanCallback;
    private boolean mScanning;
    private ImageView stop;
    private ProgressBar progressBar;

    private RecyclerView devicesView;
    private List<String> favoriteAddrs;

    private BluetoothAdapter.LeScanCallback leScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            devicesAdapter.addDevice(device, rssi);
                            devicesAdapter.notifyDataSetChanged();
                            if (favoriteAddrs.contains(device.getAddress())) {
                                connectToDevice(device);
                            }
                        }
                    });
                }
            };

    private ScanCallback createScanCallBack() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    super.onScanResult(callbackType, result);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        devicesAdapter.addDevice(result.getDevice(), result.getRssi());
                        if (favoriteAddrs.contains(result.getDevice().getAddress())) {
                            connectToDevice(result.getDevice());
                        }
                    }
                    devicesAdapter.notifyDataSetChanged();
                }

                @Override
                public void onBatchScanResults(List<ScanResult> results) {
                    super.onBatchScanResults(results);
                }

                @Override
                public void onScanFailed(int errorCode) {
                    super.onScanFailed(errorCode);
                }
            };
        }

        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_laucher);
        initViews();
        handler = new Handler();
        scanCallback = createScanCallBack();
        CSVController csvController = new CSVController();
        csvController.writeToFile(true, null);
        csvController.deleteWrote();
    }

    @Override
    protected void onResume() {
        super.onResume();
        favoriteAddrs = PreferenceUtils.getFavs(getApplicationContext());
        checkBluetoothBLE();
        checkBLEPermission();
    }

    @Override
    protected void onPause() {
        super.onPause();
        scanLeDevice(false);
        if (devicesAdapter != null) {
            devicesAdapter.clear();
        }
    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(getString(R.string.app_name));
        devicesView = (RecyclerView) findViewById(R.id.devices);
        devicesView.setLayoutManager(new LinearLayoutManager(this));

        stop = (ImageView) findViewById(R.id.stop_search);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mScanning) {
                    mScanning = false;
                    progressBar.setVisibility(View.GONE);
                    stop.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_start));
                } else {
                    mScanning = true;
                    scanLeDevice(true);
                    progressBar.setVisibility(View.VISIBLE);
                    stop.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_stop));
                }
            }
        });
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void checkBLEPermission() {

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            createAppFolder();
            setupBluetooth();
        }
    }

    private void checkBluetoothBLE() {
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.not_support_ble), Snackbar.LENGTH_INDEFINITE).show();
            finish();
        }
    }

    private void setupBluetooth() {
        boolean bluetoothEnable = isBluetoothEnable();
        if (!bluetoothEnable) {
            final Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            return;
        }

        boolean locationEnable = isLocationServiceEnabled();
        if (!locationEnable) {
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(myIntent, REQUEST_ENABLE_LOCATION);
            return;
        }

        init();
    }

    private boolean isBluetoothEnable() {
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();

        if (bluetoothAdapter == null) {
            Snackbar.make(findViewById(android.R.id.content), getString(R.string.not_support_bluetooth), Snackbar.LENGTH_INDEFINITE).show();
            finish();
        }
        return bluetoothAdapter.isEnabled();
    }

    private boolean isLocationServiceEnabled() {
        LocationManager locationManager = null;
        boolean gps_enabled = false, network_enabled = false;

        if (locationManager == null)
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
            Fabric.getLogger().e(TAG, ex.getMessage());
        }

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
            Fabric.getLogger().e(TAG, ex.getMessage());
        }

        return gps_enabled || network_enabled;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    createAppFolder();
                } else {
                    // TODO add an snackbar
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                if (isLocationServiceEnabled()) {
                    init();
                } else {
                    setupBluetooth();
                }
            }
        } else if (requestCode == REQUEST_ENABLE_LOCATION) {
            if (resultCode == RESULT_OK) {
                if (isLocationServiceEnabled() && isBluetoothEnable()) {
                    init();
                } else {
                    setupBluetooth();
                }
            }
        }
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    stopScan();
                }
            }, SCAN_PERIOD);

            mScanning = true;
            startScan();
        } else {
            mScanning = false;
            stopScan();
        }
    }


    private void init() {
        if (devicesAdapter == null) {
            devicesAdapter = new BLEDevicesAdapter(this);
            devicesView.setAdapter(devicesAdapter);
        }

        scanLeDevice(true);
    }

    private void stopScan() {
        if (bluetoothAdapter != null) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
                bluetoothLeScanner.stopScan(scanCallback);
            } else {
                bluetoothAdapter.stopLeScan(leScanCallback);
            }

            progressBar.setVisibility(View.GONE);
            stop.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_start));
        }
    }

    private void startScan() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
            bluetoothLeScanner.startScan(scanCallback);
        } else {
            bluetoothAdapter.startLeScan(leScanCallback);
        }
        progressBar.setVisibility(View.VISIBLE);
        stop.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_stop));
    }

    private void createAppFolder() {
        File f = new File(android.os.Environment.getExternalStorageDirectory(),
                File.separator + BaseUtils.FILES);
        if (!f.exists()) {
            f.mkdirs();
        }

        f = new File(android.os.Environment.getExternalStorageDirectory(),
                File.separator + BaseUtils.TEMP);
        if (!f.exists()) {
            f.mkdirs();
        }
    }

    @Override
    public void connectToDevice(BluetoothDevice device) {
        if (device == null) return;
        final Intent intent = new Intent(this, RecordActivity.class);
        intent.putExtra(RecordActivity.EXTRAS_DEVICE_NAME, device.getName());
        intent.putExtra(RecordActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());
        if (mScanning) {
            stopScan();
        }
        startActivity(intent);
    }
}
