package fub.l2d.heartratebluetooth;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.realm.Realm;

/**
 * Created by duc-d on 2/12/2017.
 */

public class HRApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        initReaml();
        initDBViewer();
    }

    private void initReaml() {
        Realm.init(getApplicationContext());
    }

    private void initDBViewer() {
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());
    }
}
