package fub.l2d.heartratebluetooth.controller;

import android.os.Handler;

import java.io.FileWriter;
import java.io.IOException;

import fub.l2d.heartratebluetooth.model.RR;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by duc-d on 2/12/2017.
 */

public class CSVController {
    public static final int FINISH_CSV_FILE = 200;
    private static final String TAG = CSVController.class.getSimpleName();
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";

    public void writeToFile(boolean delete, Handler handler) {
        RealmController realmController = new RealmController();
        realmController.initRealm();
        Realm realm = realmController.getRealm();
        final RealmResults<RR> rrs = realm.where(RR.class).
                equalTo("write", false)
                .findAll()
                .sort("timestamp");
        FileWriter fileWriter = null;

        if (rrs != null && rrs.size() > 0) {
            String fileName = rrs.get(0).fileName;
            try {
                fileWriter = new FileWriter(fileName, true);
                realm.beginTransaction();
                for (RR rr : rrs) {
                    fileWriter.append(Long.toString(rr.timestamp));
                    fileWriter.append(COMMA_DELIMITER);
                    fileWriter.append(Integer.toString(rr.rr));
                    fileWriter.append(NEW_LINE_SEPARATOR);
                    rr.write = true;
                }
                realm.commitTransaction();

                if (delete) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.deleteAll();
                        }
                    });
                }
                realm.close();
            } catch (IOException e) {
                e.printStackTrace();
                Fabric.getLogger().e(TAG, e.getMessage());
            } finally {
                try {
                    fileWriter.flush();
                    fileWriter.close();
                } catch (IOException e) {
                    System.out.println("Error while flushing/closing fileWriter !!!");
                    e.printStackTrace();
                }

            }
        } else {
            realm.close();
        }
        if (handler != null) {
            handler.sendEmptyMessage(FINISH_CSV_FILE);
        }
    }

    public void deleteWrote() {
        RealmController realmController = new RealmController();
        realmController.initRealm();
        Realm realm = realmController.getRealm();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                final RealmResults<RR> rrs = realm.where(RR.class).
                        equalTo("write", true)
                        .findAll();
                if (rrs != null && rrs.size() > 0) {
                    rrs.deleteAllFromRealm();
                }
            }
        });

        realm.close();
    }
}
