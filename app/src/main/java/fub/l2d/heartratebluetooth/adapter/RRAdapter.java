package fub.l2d.heartratebluetooth.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import fub.l2d.heartratebluetooth.R;
import fub.l2d.heartratebluetooth.model.RR;
import fub.l2d.heartratebluetooth.util.BaseUtils;

/**
 * Created by duc-d on 2/10/2017.
 */

public class RRAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<RR> rrs;
    private Context context;

    public RRAdapter(List<RR> rrs) {
        this.rrs = rrs;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (this.context == null) {
            this.context = parent.getContext();
        }
        View view = LayoutInflater.from(this.context).inflate(R.layout.rr_item, parent, false);
        return new RRViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RRViewHolder rrViewHolder = (RRViewHolder) holder;
        RR rr = rrs.get(position);

        rrViewHolder.date.setText(BaseUtils.formatDate(BaseUtils.fromLong(rr.timestamp)));
        rrViewHolder.rr.setText(Integer.toString(rr.rr));
        if (rr.isNewValue) {
            rrViewHolder.date.setTextColor(ContextCompat.getColor(context, R.color.red));
            rrViewHolder.rr.setTextColor(ContextCompat.getColor(context, R.color.red));
        } else {
            rrViewHolder.rr.setTextColor(ContextCompat.getColor(context, R.color.black));
            rrViewHolder.date.setTextColor(ContextCompat.getColor(context, R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        return rrs == null ? 0 : rrs.size();
    }

    class RRViewHolder extends RecyclerView.ViewHolder {

        public TextView date;
        public TextView rr;

        public RRViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.rr_date);
            rr = (TextView) itemView.findViewById(R.id.rr_item);
        }
    }
}
