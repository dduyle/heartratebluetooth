package fub.l2d.heartratebluetooth.model;

/**
 * Created by duyle on 2/8/17.
 */

public class Device {
    public String name;
    public String addr;

    public Device(String mDeviceName, String mDeviceAddress) {
        this.name = mDeviceName;
        this.addr = mDeviceAddress;
    }

}
