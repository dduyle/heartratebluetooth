package fub.l2d.heartratebluetooth.controller;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by duc-d on 2/12/2017.
 */

public class Migration implements RealmMigration {
    public static final int DB_VERSION = 0;

    private static final int HASH_CODE = 200;

    @Override
    public int hashCode() {
        return HASH_CODE;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Migration;
    }

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();
    }
}
