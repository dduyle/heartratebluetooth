package fub.l2d.heartratebluetooth.ui.iviews;

import android.bluetooth.BluetoothDevice;

/**
 * Created by duyle on 2/8/17.
 */

public interface ILaucher {
    void connectToDevice(BluetoothDevice device);
}
