package fub.l2d.heartratebluetooth.model;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by duc-d on 2/10/2017.
 */

public class RR extends RealmObject {
    public long timestamp;
    public int rr;
    public boolean isNewValue;
    public String fileName;
    public boolean write;

    public RR() {
    }

    public RR(int rr, long timestamp) {
        this.rr = rr;
        this.timestamp = timestamp;
        this.isNewValue = true;
    }
}
