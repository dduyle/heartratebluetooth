package fub.l2d.heartratebluetooth.controller;

import android.util.Log;

import java.util.List;

import fub.l2d.heartratebluetooth.model.RR;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by duc-d on 2/12/2017.
 */

public class RealmController {
    private static final String TAG = RealmController.class.getSimpleName();
    private Realm realm;

    public void initRealm() {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(Migration.DB_VERSION)
                .migration(new Migration())
                .deleteRealmIfMigrationNeeded() //TO REMOVE THIS IN PROD
                .build();
        realm = Realm.getInstance(realmConfiguration);
    }

    public void saveRRs(final List<RR> rrs) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    initRealm();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealm(rrs);
                        }
                    });
                    realm.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    Fabric.getLogger().e(TAG, e.getMessage());
                }
            }
        }).start();

    }

    public Realm getRealm() {
        return realm;
    }
}
