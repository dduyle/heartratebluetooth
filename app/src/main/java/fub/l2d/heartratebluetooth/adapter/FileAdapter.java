package fub.l2d.heartratebluetooth.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import fub.l2d.heartratebluetooth.R;
import fub.l2d.heartratebluetooth.model.FileRR;

/**
 * Created by duc-d on 2/13/2017.
 */

public class FileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<FileRR> files;

    public FileAdapter(List<FileRR> files) {
        this.files = files;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.file_item, parent, false);
        return new FileViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {
        final FileViewHolder holder = (FileViewHolder) viewholder;
        final FileRR file = files.get(position);

        holder.nameView.setText(file.filename);
        holder.checkBox.setChecked(file.checked);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file.checked = !file.checked;
                holder.checkBox.setChecked(file.checked);
            }
        });

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file.checked = !file.checked;
                holder.checkBox.setChecked(file.checked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return files == null ? 0 : files.size();
    }

    class FileViewHolder extends RecyclerView.ViewHolder {

        TextView nameView;
        CheckBox checkBox;

        public FileViewHolder(View itemView) {
            super(itemView);
            nameView = (TextView) itemView.findViewById(R.id.file_name);
            checkBox = (CheckBox) itemView.findViewById(R.id.file_check);
        }
    }
}
