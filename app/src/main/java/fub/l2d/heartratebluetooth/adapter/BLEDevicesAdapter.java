package fub.l2d.heartratebluetooth.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fub.l2d.heartratebluetooth.R;
import fub.l2d.heartratebluetooth.ui.iviews.ILaucher;

/**
 * Created by duyle on 2/7/17.
 */

public class BLEDevicesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<BluetoothDevice> devices;
    private Context context;
    private ILaucher laucherActivity;

    private final HashMap<BluetoothDevice, Integer> rssiMap = new HashMap<BluetoothDevice, Integer>();


    public BLEDevicesAdapter(ILaucher laucherActivity) {
        devices = new ArrayList<>();
        this.laucherActivity = laucherActivity;
    }

    public void addDevice(BluetoothDevice device, int rssi) {
        if (!devices.contains(device)) {
            devices.add(device);
        }
        rssiMap.put(device, rssi);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ble_device_item, parent, false);
        context = parent.getContext();
        return new BLEDeviceHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final BluetoothDevice device = devices.get(position);

        BLEDeviceHolder viewHolder = (BLEDeviceHolder) holder;
        final String deviceName = device.getName();
        if (deviceName != null && deviceName.length() > 0) {
            viewHolder.deviceName.setText(deviceName);
        } else {
            viewHolder.deviceName.setText(context.getString(R.string.unknown_device));
        }

        viewHolder.deviceAddr.setText(device.getAddress());
        viewHolder.deviceRssi.setText("" + rssiMap.get(device) + " dBm");

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                laucherActivity.connectToDevice(device);
            }
        });

    }

    @Override
    public int getItemCount() {
        return devices == null ? 0 : devices.size();
    }

    public void clear() {
        devices.clear();
        notifyDataSetChanged();
    }

    class BLEDeviceHolder extends RecyclerView.ViewHolder {

        public TextView deviceName;
        public TextView deviceAddr;
        public TextView deviceRssi;

        public BLEDeviceHolder(View itemView) {
            super(itemView);
            deviceName = (TextView) itemView.findViewById(R.id.ble_device_name);
            deviceAddr = (TextView) itemView.findViewById(R.id.ble_device_addr);
            deviceRssi = (TextView) itemView.findViewById(R.id.ble_device_rssi);
        }
    }
}
